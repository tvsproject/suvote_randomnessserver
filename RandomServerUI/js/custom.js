//Prepare progress bars
$(function () {
		//Progress bar for key files
    var progressbarFiles = $("#progressbarFiles"),
        progressbarFile = $("#progressbarFile");	
    var progressLabelFile = $(".progress-labelFile"),
        progressLabelFiles = $(".progress-labelFiles");

		//Progress bar for showing progress through keys to process
    progressbarFiles.progressbar({
        value: false,
        change: function () {
            progressLabelFiles.text(progressbarFiles.progressbar("value") + "%");
        },
        complete: function () {
            //progressLabelFiles.text( "Complete!" );
        }
    });

		//progress bar to show progress through the processing of an individual key
    progressbarFile.progressbar({
        value: false,
        change: function () {
            progressLabelFile.text(progressbarFile.progressbar("value") + "%");
        },
        complete: function () {
            //progressLabelFile.text( "Complete!" );
        }
    });
    progressbarFiles.progressbar("value", 0);
    progressbarFile.progressbar("value", 0);
});

var exampleSocket;
var localPort;
//Get the updated local port - generated on startup
function updateAndConnect(){
	$.getJSON('js/localport.json', function(data) {
		localPort = data.port;
		connect();
	});
}

//Connect to the server
function connect() {
    exampleSocket = new WebSocket("ws://localhost:" + localPort);
    exampleSocket.onclose = function (evt) {
        onClose(evt)
    };
    exampleSocket.onerror = function (evt) {
        onError(evt)
    };
    exampleSocket.onmessage = function (event) {
        processMessage(event)
    };
    exampleSocket.onopen = function (event) {
        document.getElementById("status").innerHTML = "Connected";
    };
}

//Call to start the generate ballots or reconnect to a running generation
function genBallots() {
    exampleSocket.send("{'type':'genRandomness'}");
}

//Call to start the generate ballots or reconnect to a running generation
function getFailures() {
    exampleSocket.send("{'type':'getFailures'}");
}

//Process an incoming message
function processMessage(event) {
    var msg = JSON.parse(event.data);
    if (msg.type == "progressUpdate") {
				//Only type of message is a progressUpdate, only variation is which progress bar
        if (msg.progressID == "fileProgress") {
            $("#progressbarFile").progressbar("value", msg.value + 0.0);

        } else if (msg.progressID == "filesProgress") {
            $("#progressbarFiles").progressbar("value", msg.value + 0.0);

        }
    }else if (msg.type == "getFailures"){
			if(msg.msg.length>0){
				var finalList ="";				
				for(var i=0;i<msg.msg.length;i++){
					finalList = finalList + msg.msg[i]+"\n";
				}
				document.getElementById("failures").innerHTML = finalList;
			}else{
				alert("No Failures Recorded");
			}
		}else if (msg.type == "error"){
			alert("An Error has occured\n" + msg.msg);
		}  
}

function onClose(evt)

{
    document.getElementById("status").innerHTML = "Disconnected";
    console.log("DISCONNECTED");
}

function onError(evt) {
    console.log(evt.data);
}
