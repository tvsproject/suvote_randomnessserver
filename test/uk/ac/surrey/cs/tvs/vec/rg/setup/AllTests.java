package uk.ac.surrey.cs.tvs.vec.rg.setup;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The class <code>AllTests</code> builds a suite that can be used to run all of the tests within its package as well as within any
 * subpackages of its package.
 * 
 * @generatedBy CodePro at 05/11/13 16:03
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ RGKeyGenerationTest.class, })
public class AllTests {
}
